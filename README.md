# Cova.ReduxN

Unidirectional data flow based on: Redux, Vuex, ReduxSimple, flux; Implemented with key elements:
* Multi threading support
* Ensuring sequential state change through queues
* 2 kinds of commits: High priority & normal
* With rx support